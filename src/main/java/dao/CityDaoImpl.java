package dao;

import model.City;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CityDaoImpl implements CityDao {

    @Override
    public void save(City city) throws NullPointerException {

        System.out.println(city.toString());

        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()){
            Transaction transaction = session.beginTransaction();
            session.save(city);
            transaction.commit();
        }
    }

    @Override
    public List<City> getSortedCities() {
        List<City> sortedList;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Query<City> query = session.createQuery("FROM City ORDER BY LOWER(name) DESC", City.class);
        sortedList = query.getResultList();
        transaction.commit();
        return sortedList;
    }

    @Override
    public List<City> getGroupingCities() {
        List<City> sortedList;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Query<City> query = session.createQuery("FROM City ORDER BY district DESC ", City.class);
        sortedList = query.getResultList();
        transaction.commit();
        return sortedList;
    }

    @Override
    public List<City> getAllCities() {
        List<City> cities;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Query<City> allCities = session.createQuery("FROM City ", City.class);
        cities = allCities.getResultList();
        transaction.commit();
        return cities;
    }

    @Override
    public String getMaxPopulation() {
        City city = Collections.max(getAllCities(), (o1, o2) -> Integer.compare(o1.getPopulation(), o2.getPopulation()));
        return "[" + city.getId() + "] = " + city.getPopulation();
    }

    @Override
    public List<String> getAllRegions() {
        List<City> cities = getAllCities();
        List<String> regions = new ArrayList<>();
        cities.stream().collect(Collectors.groupingBy(City::getRegion, Collectors.counting())).forEach((a, b) ->
                regions.add(a +" - " + b));
        return regions;
    }
}
