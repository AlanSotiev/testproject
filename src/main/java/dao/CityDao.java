package dao;

import model.City;

import java.util.List;

public interface CityDao {
    void save(City city);
    List<City> getSortedCities();
    List<City> getGroupingCities();
    String getMaxPopulation();
    List<String> getAllRegions();
    List<City> getAllCities();
}
