import dao.CityDaoImpl;
import model.City;

import java.io.*;
import java.util.*;

public class Main {

    static List<City> cities;
    static CityDaoImpl dao;

    public static void main(String[] args) throws FileNotFoundException {
        dao = new CityDaoImpl();
        cities = new ArrayList<>();
        Scanner scanner = new Scanner(new FileReader("./src/main/resources/Cities.txt"));

        while(scanner.hasNext()){
            String[] line = scanner.nextLine().split(";");
            City city = new City(line[1],line[2],line[3],Integer.parseInt(line[4]),Integer.parseInt(line[5]));
            if (cities.contains(city))
                continue;
            dao.save(city);
            cities.add(city);
        }

        scanner.close();

        System.out.println("Выберите требумое действие:\n" +
                           "1) Список городов\n" +
                           "2) Отсортированный список городов по убыванию с учётом регистра\n" +
                           "3) Отсортированный список городов по федеральному округу, по убыванию\n" +
                           "4) Город с наибольшим количеством жителей\n" +
                           "5) Количество городов в разрезе регионов\n" +
                           "...\n" +
                           "n) Выход\n");

        Scanner scanner1 = new Scanner(System.in);

        while (scanner1.hasNext()){
            String x = scanner1.next();
            if (!(x.equals("n"))) {
                if (x.equals("1")) {
                    dao.getAllCities().forEach(System.out::println);
                } else if (x.equals("2")) {
                    dao.getSortedCities().forEach(System.out::println);
                } else if (x.equals("3")) {
                    dao.getGroupingCities().forEach(System.out::println);
                } else if (x.equals("4")) {
                    System.out.println(dao.getMaxPopulation());
                } else if (x.equals("5")) {
                    dao.getAllRegions().forEach(System.out::println);
                } else {
                    System.out.println("Некорректный ввод!");
                }
            }else
                break;

        }
        scanner1.close();
    }
}
