package model;

import lombok.*;


import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "CITY")
public class City {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REGION")
    private String region;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "POPULATION")
    private int population;

    @Column(name = "FOUNDATION")
    private int foundation;

    public City() {
    }

    public City(String name, String region, String district, int population, int foundation) {
        this.name = name;
        this.region = region;
        this.district = district;
        this.population = population;
        this.foundation = foundation;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", district='" + district + '\'' +
                ", population=" + population +
                ", foundation=" + foundation +
                '}';
    }
}